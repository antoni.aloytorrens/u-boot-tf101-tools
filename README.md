# U-Boot Asus Eee Pad Transformer TF101 Tools

* **Warning**: These tools won't work on TF101G or SL101 devices!

---

## Setup

Put your TF101 tablet in APX mode (Power + Volume Up and wait 6 seconds) and connect it to your PC using the [USB to 40 Pin cable](https://imgur.com/a/mfiryeP).

* If your tablet is SBKv1 version: run `uboot_sbkv1.sh` in your PC.
* If your tablet is SBKv2 version: run `uboot_sbkv2.sh` in your PC.

---

## Media

* [How to install Debian 11 Bullseye on the Asus Eee Pad Transformer TF101 (U-Boot Method)](https://youtu.be/Rb8rVW_74w0).

---

## Special Thanks

Thanks to [Clamor](https://github.com/clamor-s/) for getting U-Boot working on the Asus Eee Pad Transformer TF101.
